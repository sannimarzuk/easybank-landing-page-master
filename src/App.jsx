import { useEffect, useState } from "react";
import "./App.scss";
import Logo from "./images/logo.svg";
import LogoF from "./images/logo-white.svg";
import * as FaIcons from "react-icons/fa";
import Budgeting from "./images/icon-budgeting.svg";
import Onboarding from "./images/icon-onboarding.svg";
import API from "./images/icon-api.svg";
import Online from "./images/icon-online.svg";

function App() {
  const [active, setActive] = useState(false);
  const [scrolled, setScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const scrollDistance =
        document.body.scrollTop || document.documentElement.scrollTop;
      setScrolled(scrollDistance > 20);
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [scrolled]);

  return (
    <>
      <header className={scrolled ? "header scrolled" : "header"}>
        <div className="header__container">
          <a href="index.html" className="logo">
            <img src={scrolled ? LogoF : Logo} alt="Logo" />
          </a>
          <nav>
            <button
              className={active ? "menu__btn close" : "menu__btn"}
              onClick={() => setActive(!active)}
            >
              <span className="sr-only">Menu</span>
            </button>
            <ul className={active ? "list show" : "list"}>
              <li className="list__item">
                <a className="nav__links" href="#">
                  Home
                </a>
              </li>
              <li className="list__item">
                <a className="nav__links" href="#">
                  About
                </a>
              </li>
              <li className="list__item">
                <a className="nav__links" href="#">
                  Contact
                </a>
              </li>
              <li className="list__item">
                <a className="nav__links" href="#">
                  Blog
                </a>
              </li>
              <li className="list__item">
                <a className="nav__links" href="#">
                  Careers
                </a>
              </li>
            </ul>
          </nav>
          <a className="invite__btn" href="#">
            Request Invite
          </a>
        </div>
      </header>
      {active && <div aria-hidden="true" className="overlay"></div>}
      <main>
        <section className="hero__section">
          <div className="hero__illustration"></div>
          <div className="hero__text">
            <h1> Next generation digital banking</h1>
            <p>
              Take your financial life online. Your Easybank account will be a
              one-stop-shop for spending, saving, budgeting, investing, and much
              more.
            </p>
            <a href="#" className="invite__btn">
              Request Invite
            </a>
          </div>
        </section>
        <section className="features__section">
          <div className="top">
            <h2 className="feature__heading">Why choose Easybank?</h2>
            <p>
              We leverage Open Banking to turn your bank account into your
              financial hub. Control your finances like never before.
            </p>
          </div>
          <div className="bottom">
            <div className="feature">
              <img src={Online} alt="feature - img" width="72" height="72" />
              <div className="feature__description">
                <h3>Online Banking</h3>
                <p>
                  Our modern web and mobile applications allow you to keep track
                  of your finances wherever you are in the world.
                </p>
              </div>
            </div>

            <div className="feature">
              <img src={Budgeting} alt="feature - img" width="72" height="72" />
              <div className="feature__description">
                <h3>Simple Budgeting</h3>
                <p>
                  See exactly where your money goes each month. Receive
                  notifications when you’re close to hitting your limits.
                </p>
              </div>
            </div>

            <div className="feature">
              <img
                src={Onboarding}
                alt="feature - img"
                width="72"
                height="72"
              />
              <div className="feature__description">
                <h3>Fast Onboarding</h3>
                <p>
                  We don’t do branches. Open your account in minutes online and
                  start taking control of your finances right away.
                </p>
              </div>
            </div>

            <div className="feature">
              <img src={API} alt="feature - img" width="72" height="72" />
              <div className="feature__description">
                <h3>Open API</h3>
                <p>
                  Manage your savings, investments, pension, and much more from
                  one account. Tracking your money has never been easier.
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="article__section">
          <h2 className="article__heading"> Latest Articles</h2>
          <div className="articles__container">
            <article>
              <div className="article__image"></div>
              <blockquote className="article__text">
                <cite>By Claire Robinson</cite>
                <h3> Receive money in any currency with no fees</h3>
                <p>
                  The world is getting smaller and we’re becoming more mobile.
                  So why should you be forced to only receive money in a single
                  …
                </p>
              </blockquote>
            </article>
            <article>
              <div className="article__image second"></div>
              <blockquote className="article__text">
                <cite> By Wilson Hutton</cite>
                <h3> Treat yourself without worrying about money</h3>
                <p>
                  Our simple budgeting feature allows you to separate out your
                  spending and set realistic limits each month. That means you …
                </p>
              </blockquote>
            </article>
            <article>
              <div className="article__image third"></div>
              <blockquote className="article__text">
                <cite> By Wilson Hutton</cite>
                <h3> Take your Easybank card wherever you go</h3>
                <p>
                  We want you to enjoy your travels. This is why we don’t charge
                  any fees on purchases while you’re abroad. We’ll even show you
                  …
                </p>
              </blockquote>
            </article>
            <article>
              <div className="article__image fourth"></div>
              <blockquote className="article__text">
                <cite> By Claire Robinson</cite>
                <h3> Our invite-only Beta accounts are now live!</h3>
                <p>
                  After a lot of hard work by the whole team, we’re excited to
                  launch our closed beta. It’s easy to request an invite through
                  the site ...
                </p>
              </blockquote>
            </article>
          </div>
        </section>
      </main>
      <footer>
        <div className="top">
          <a href="index.html" className="footer-logo">
            <img src={LogoF} alt="Logo" />
          </a>
          <div className="social__links">
            <a href="#" aria-label="Facebook Link">
              <FaIcons.FaFacebookSquare width={21} height={18} />
            </a>
            <a href="#" aria-label="Youtube Link">
              <FaIcons.FaYoutube width={21} height={18} />
            </a>
            <a href="#" aria-label="Twitter Link">
              <FaIcons.FaTwitter width={21} height={18} />
            </a>
            <a href="#" aria-label="Pinterest Link">
              <FaIcons.FaPinterest width={21} height={18} />
            </a>
            <a href="#" aria-label="Instagram Link">
              <FaIcons.FaInstagram width={21} height={18} />
            </a>
          </div>
        </div>

        <ul className="footer__links">
          <li>
            <a href="index.html"> About Us</a>
          </li>
          <li>
            <a href="index.html"> Contact</a>
          </li>
          <li>
            <a href="index.html"> Blog</a>
          </li>
          <li>
            <a href="index.html"> Careers</a>
          </li>
          <li>
            <a href="index.html"> Support</a>
          </li>
          <li>
            <a href="index.html"> Privacy Policy</a>
          </li>
        </ul>
        <div className="bottom">
          <a className="invite__btn" href="#">
            Request Invite
          </a>
          <p>© Easybank. All Rights Reserved</p>
        </div>
      </footer>
    </>
  );
}

export default App;
